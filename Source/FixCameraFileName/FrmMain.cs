﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using MediaInfoLib;

namespace FixCameraFileName
{

    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (this.dlgBrowse.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                this.clearOldData();
                this.fillDatatable(this.dlgBrowse.FileNames);
            }
        }

        private void clearOldData()
        {
            this.dgvFiles.Rows.Clear();
            this.pbPreview.Image = null;
            this.pbPreview.Refresh();
        }

        private string getNewName(string path)
        {
            FileInfo fi = new FileInfo(path);
            if (fi.Extension == ".jpg" || fi.Extension == ".JPG")
            {
                return this.getNewNameForJpg(path);
            }
            else if (fi.Extension == ".mp4")
            {
                return this.getNewNameForMp4(path);
            }
            return "";
        }

        private string getNewNameForJpg(string path)
        {
            try
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                Image myImage = Image.FromStream(fs, false, false);
                PropertyItem propItem = myImage.GetPropertyItem(36867);
                string dateTaken = Encoding.UTF8.GetString(propItem.Value);
                fs.Close();
                string[] dateTakenParts = dateTaken.Split(' ');
                dateTakenParts[0] = dateTakenParts[0].Replace(':', '-');
                DateTime dt = DateTime.Parse(dateTakenParts[0] + " " + dateTakenParts[1]);

                PersianCalendar pc = new PersianCalendar();
                StringBuilder result = new StringBuilder();
                result.Append(pc.GetYear(dt));
                result.Append('_');
                result.Append(pc.GetMonth(dt));
                return String.Format(
                    "{0}_{1,2:D2}_{2,2:D2} - {3:D2}_{4:D2}_{5:D2}{6}",
                    pc.GetYear(dt),
                    pc.GetMonth(dt),
                    pc.GetDayOfMonth(dt),
                    pc.GetHour(dt),
                    pc.GetMinute(dt),
                    pc.GetSecond(dt),
                    new FileInfo(path).Extension
                );
            }
            catch
            {
                return "";
            }
        }

        private string getNewNameForMp4(string path)
        {
            try
            {
                FileInfo fi = new FileInfo(path);
                MediaInfo mi = new MediaInfo();
                mi.Open(path);
                string[] dataLines = mi.Inform().Split('\n');
                foreach (string line in dataLines)
                {
                    if (line.StartsWith("Encoded date"))
                    {
                        // parse `encoded date` tag:
                        // >Encoded date                             : UTC 2014-07-29 15:21:29
                        string newLine = line.Substring(line.IndexOf(':') + 1);    // > UTC 2014-07-29 15:21:29
                        newLine = newLine.Replace("UTC", "");                       // >  2014-07-29 15:21:29
                        newLine = newLine.Trim();                                   // >2014-07-29 15:21:29
                        //newLine = newLine.Replace(':', '-');                      // >2014-07-29 15-21-29
                        DateTime dt = DateTime.Parse(newLine);
                        PersianCalendar pc = new PersianCalendar();
                        StringBuilder result = new StringBuilder();
                        result.Append(pc.GetYear(dt));
                        result.Append('_');
                        result.Append(pc.GetMonth(dt));
                        return String.Format(
                            "{0}_{1,2:D2}_{2,2:D2} - {3:D2}_{4:D2}_{5:D2}{6}",
                            pc.GetYear(dt),
                            pc.GetMonth(dt),
                            pc.GetDayOfMonth(dt),
                            pc.GetHour(dt),
                            pc.GetMinute(dt),
                            pc.GetSecond(dt),
                            fi.Extension
                        );
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";
            }
        }

        private void fillDatatable(string[] lstFiles)
        {
            for (int i = 0, len = lstFiles.Length; i < len; ++i)
            {
                this.dgvFiles.Rows.Add(
                    lstFiles[i],
                    new FileInfo(lstFiles[i]).Name,
                    this.getNewName(lstFiles[i])
                );
            }
        }

        private void dgvFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            string path = this.dgvFiles[this.colPath.Index, e.RowIndex].Value.ToString();
            FileInfo fi = new FileInfo(path);
            try
            {
                if (fi.Extension == ".jpg" || fi.Extension == ".JPG")
                {
                    this.pbPreview.Load(path);
                }
                else
                {
                    this.pbPreview.Image = null;
                    this.pbPreview.Refresh();
                }
            }
            catch
            {
                this.pbPreview.Image = null;
                this.pbPreview.Refresh();
            }
        }

        private void dgvFiles_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.dgvFiles_CellContentClick(sender, e);
        }

        private void btnFix_Click(object sender, EventArgs e)
        {
            this.pbPreview.Image = null;
            this.pbPreview.Refresh();
            //
            int pathIndex = this.colPath.Index;
            int oldNameIndex = this.colOldName.Index;
            int newNameIndex = this.colNewName.Index;
            string destinationPath;
            for (int i = 0, len = this.dgvFiles.Rows.Count; i < len; ++i)
            {
                if (this.dgvFiles[newNameIndex, i].Value == null || this.dgvFiles[newNameIndex, i].Value.ToString().Length == 0)
                    continue;
                try
                {
                    FileInfo fi = new FileInfo(this.dgvFiles[pathIndex, i].Value.ToString());
                    destinationPath = fi.Directory.FullName + "\\" + this.dgvFiles[newNameIndex, i].Value.ToString();
                    if (!File.Exists(destinationPath))
                    {
                        File.Move(
                            this.dgvFiles[pathIndex, i].Value.ToString(),
                            destinationPath
                        );
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + this.dgvFiles[oldNameIndex, i].Value.ToString() + "\n" + ex.Message);
                }
            }
            MessageBox.Show("Done! :)");
        }

        private void lblAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new frmAbout().ShowDialog(this);
        }
    }
}
